import React, { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";
import Jumbotron from "./components/Jumbotron";

function App() {
  const [repOrSen, setRepOrSen] = useState("Senator");
  const [reps, useReps] = useState([]);
  const [formObject, setFormObject] = useState([]);
  const [selectedState, setSelectedState] = useState("AL");
  const [searchResult, setSearchResult] = useState([]);
  const [displayNamed, setDisplayName] = useState({});

  const getReps = async () => {
    const results = await axios.get(
      `http://localhost:8080/${repOrSen}s/${selectedState}`
    );
    console.log(results);
    setSearchResult(results.data.results);
  };
  const displayName = async (representative) => {
    await setDisplayName(representative);
  };

  let stateArr = [
    "AL",
    "AK",
    "AZ",
    "AR",
    "CA",
    "CO",
    "CT",
    "DE",
    "DC",
    "FL",
    "GA",
    "HI",
    "ID",
    "IL",
    "IN",
    "IA",
    "KS",
    "KY",
    "LA",
    "ME",
    "MD",
    "MA",
    "MI",
    "MN",
    "MS",
    "MO",
    "MT",
    "NE",
    "NV",
    "NH",
    "NJ",
    "NM",
    "NY",
    "NC",
    "ND",
    "OH",
    "OK",
    "OR",
    "PA",
    "RI",
    "SC",
    "SD",
    "TN",
    "TX",
    "UT",
    "VT",
    "VA",
    "WA",
    "WV",
    "WI",
    "WY",
  ];

  console.log(searchResult);
  return (
    <div>
      <div id="float-container">
        <div id="floatChild">
          <label for="senOrRep">
            <Jumbotron>
              {" "}
              <h1>
                Would you like to learn about a Senator or a Representative?
              </h1>
            </Jumbotron>
          </label>

          <select
            name="senOrRep"
            id="senOrRepSelect"
            onChange={(e) => {
              setRepOrSen(e.target.value);
            }}
          >
            <option>Senator</option>
            <option>Representative</option>
          </select>
          <select
            name="selectedState"
            id="selectedState"
            onChange={(e) => {
              setSelectedState(e.target.value);
            }}
          >
            {stateArr.map((item) => (
              <option>{item}</option>
            ))}
          </select>
          <button
            id="submitButton"
            onClick={(e) => {
              getReps();
            }}
          >
            Get My {repOrSen}
          </button>
        </div>
        <div class="table-responsive" id="floatChild">
          <table class="table-bordered">
            <tr class="table-bordered">
              <th class="table-bordered">Name</th>
              <th class="table-bordered">Party</th>
            </tr>
            {searchResult.map((item) => (
              <tr onClick={(e) => displayName(item)}>
                <td>{item.name} </td>
                <td>{item.party}</td>
              </tr>
            ))}
          </table>
        </div>
        <div class="extraInfoDiv" id="floatChild">
          <table class="table-bordered">
            <tr class="table-bordered">
              <th> Website Link </th>
              <th>Office Address</th>
              <th>PhoneNumber</th>
            </tr>
            <td class="table-bordered">
              {/* ask about this */}
              <a href={displayNamed.link} target="_blank">
                {displayNamed.link}
              </a>
            </td>
            <td class="table-bordered">{displayNamed.office}</td>
            <td class="table-bordered">{displayNamed.phone}</td>
          </table>
        </div>
      </div>
    </div>
  );
}

export default App;
